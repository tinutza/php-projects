<h3>Add or Edit ToDo Item</h3>

<?php

function getForm($desc = '', $date='', $done='') {
    //heredoc
    if($done == 1){
        $checked = "checked";
    } else {
        $checked = "";
    }
$form = <<< ENDTAG
      <form method="POST">
    Descritpion: <input type="text" name="description" value="$desc"><br><br>
    Due Date: <input type="text" name="dueDate" value="$date"><br><br>
    Is Done: <input type="checkbox" name="isDone" value="1" {$checked}><br><br>
    
    <input type ="submit" value="Save"> 
</form>  
ENDTAG;
return $form;
}



/*Tri-State form
   *1. First show
 * 2. Submission succesfull
 * 3. Submition failed
 * */

 if(!isset($_POST['description'])){
     if(isset($_GET['id'])){
         echo getForm($_GET['desc'], $_GET['date'], $_GET['done']);
     } else {
         echo getForm();
     }
 } else {
     //Receiving a submission
     $description = $_POST['description'];
     $dueDate = $_POST['dueDate'];
     if(isset($_POST['isDone'])){
         $isDone  = $_POST['isDone'];
     } else {
         $isDone  = 0;
     }
     
     //
     $errorList = array();
     if(strlen($description) <2){
         array_push($errorList, "Descritpion must be at least 2 characters long");
     }
     
     if(! preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $dueDate)){
         array_push($errorList, "Due Date must be in format YYYY-MM-DD");
     }
     //
     if($errorList){
         //submission failed
         echo "<h5>Problems  found in your submission</h5>\n";
         echo "</ul>\n";
         foreach ($errorList as $error){
             echo "<li>".htmlspecialchars($error)."</li>";
         }
         echo "</ul>\n";
         echo getForm($description, $dueDate, $isDone);
     }else {
         //submitionn succesfull
         require_once 'db.php';
          if(isset($_GET['id'])){
              $id = $_GET['id'];
           $sql = sprintf("Update todoitem set description = '%s', dueDate = '%s', isDone = '%s' where ID = '%s'", mysqli_escape_string($conn, $description),
                   mysqli_escape_string($conn,$dueDate), mysqli_escape_string($conn,$isDone), mysqli_escape_string($conn, $id));
        } else {
           $sql = "INSERT INTO todoitem VALUES (NULL, '" . mysqli_escape_string($conn, $description). "', '". mysqli_escape_string($conn,$dueDate). "', '". mysqli_escape_string($conn,$isDone)."')";
        }
    $result = mysqli_query($conn, $sql);
    if(!$result){
        echo "Error executing query [$sql] : " .mysqli_error($conn);
    } else {
        echo "Submission Succesfull";
    }
     }
 
 }
