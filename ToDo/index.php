<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <a href="itemaddedit.php">Add item</a>
        <table border="1">
            <tr>
                <th>#</th>
                <th>Description</th>
                <th>Due Date</th>
                <th>is Done</th>
                <th>Operations</th>
            </tr>
      <?php
      require_once 'db.php';

$sql = "SELECT * FROM todoitem";

$result = mysqli_query($conn, $sql);
if (!$result) {
    echo "Error executing query [ $sql ] : " . mysqli_error($conn);
    exit;
}
$dataRows = mysqli_fetch_all($result, MYSQLI_ASSOC);

// print_r($dataRows);

foreach ($dataRows as $row) {
    // TODO: htmlentities() or htmlspecialchars() [PREFERRED]
    $ID = $row['ID'];
    $description = htmlspecialchars($row['description']);
    $dueDate = $row['dueDate'];
    $isDone = $row['isDone'];
    echo "<tr><td>$ID</td><td>$description</td><td>$dueDate</td><td>$isDone</td>";
            echo "<td><a href=\"itemdelete.php?id=$ID\">Delete</a></td>";
            echo "<td><a href=\"itemaddedit.php?id=$ID&desc=$description&date=$dueDate&done=$isDone\">Update</a></td>";
            echo "</tr>\n";
}
        ?>
        </table>
    </body>
</html>
