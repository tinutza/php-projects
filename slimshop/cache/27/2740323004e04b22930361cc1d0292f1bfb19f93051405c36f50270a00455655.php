<?php

/* error_internal.html.twig */
class __TwigTemplate_29d36837ddfcc155c22fb74fb6dfca6c66adcf4bee08b0f0aefc194fb0b2a150 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "error_internal.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Internal Error ";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <h1>Internal error</h1>
        <p>We are very sory, we failed to fulfill your request. Our team o coing has already been contacted </p>
        <p><a href=\"/\">Click to continue</a></p>
";
    }

    public function getTemplateName()
    {
        return "error_internal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"master.html.twig\" %}

{% block title %} Internal Error {% endblock %}

{% block content %}
    <h1>Internal error</h1>
        <p>We are very sory, we failed to fulfill your request. Our team o coing has already been contacted </p>
        <p><a href=\"/\">Click to continue</a></p>
{% endblock %}";
    }
}
