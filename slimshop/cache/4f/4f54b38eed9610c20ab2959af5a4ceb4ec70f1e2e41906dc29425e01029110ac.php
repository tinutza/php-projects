<?php

/* order.html.twig */
class __TwigTemplate_00913d1e8adf079262af0d9c395d8da9dbebdc1171de8b22ad8d1eec30b5116f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "order.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "    
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Cart";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "    <p>Total value before tax is ";
        echo twig_escape_filter($this->env, (isset($context["totalBeforeTax"]) ? $context["totalBeforeTax"] : null), "html", null, true);
        echo "</p>
    <p>Shipping before tax is ";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["shippingBeforeTax"]) ? $context["shippingBeforeTax"] : null), "html", null, true);
        echo "</p>
    <p>Taxes ";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["taxes"]) ? $context["taxes"] : null), "html", null, true);
        echo "</p>
    <p>Final total with shipping and taxes<b> ";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["totalWithShippingAndTaxes"]) ? $context["totalWithShippingAndTaxes"] : null), "html", null, true);
        echo "</b></p>
    <form method = \"POST\">
        Name: <input type=\"text\" name=\"name\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sessionUser"]) ? $context["sessionUser"] : null), "name", array()), "html", null, true);
        echo "\"><br>
        Address: <input type=\"text\" name=\"address\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sessionUser"]) ? $context["sessionUser"] : null), "address", array()), "html", null, true);
        echo "\"><br>
        Postal Code: <input type=\"text\" name=\"postalCode\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sessionUser"]) ? $context["sessionUser"] : null), "postalCode", array()), "html", null, true);
        echo "\"><br>
        Email: <input type=\"email\" name=\"email\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sessionUser"]) ? $context["sessionUser"] : null), "email", array()), "html", null, true);
        echo "\"><br>
        Phone Number: <input type=\"text\" name=\"phoneNumber\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sessionUser"]) ? $context["sessionUser"] : null), "phoneNumber", array()), "html", null, true);
        echo "\"><br>
        <p><u>TODO: pyement information</u></p>
        <input type=\"submit\" value=\"Place order\">
          </form>
        <a href=\"/order\"> Place Order </a>
";
    }

    public function getTemplateName()
    {
        return "order.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 19,  77 => 18,  73 => 17,  69 => 16,  65 => 15,  60 => 13,  56 => 12,  52 => 11,  47 => 10,  44 => 9,  38 => 7,  33 => 4,  30 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"master.html.twig\" %}

{% block head %}
    
{% endblock %}

{% block title %}Cart{% endblock %}

{% block content %}
    <p>Total value before tax is {{totalBeforeTax}}</p>
    <p>Shipping before tax is {{shippingBeforeTax}}</p>
    <p>Taxes {{taxes}}</p>
    <p>Final total with shipping and taxes<b> {{totalWithShippingAndTaxes}}</b></p>
    <form method = \"POST\">
        Name: <input type=\"text\" name=\"name\" value=\"{{sessionUser.name}}\"><br>
        Address: <input type=\"text\" name=\"address\" value=\"{{sessionUser.address}}\"><br>
        Postal Code: <input type=\"text\" name=\"postalCode\" value=\"{{sessionUser.postalCode}}\"><br>
        Email: <input type=\"email\" name=\"email\" value=\"{{sessionUser.email}}\"><br>
        Phone Number: <input type=\"text\" name=\"phoneNumber\" value=\"{{sessionUser.phoneNumber}}\"><br>
        <p><u>TODO: pyement information</u></p>
        <input type=\"submit\" value=\"Place order\">
          </form>
        <a href=\"/order\"> Place Order </a>
{% endblock %}";
    }
}
