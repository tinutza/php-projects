<?php

/* register.html.twig */
class __TwigTemplate_6dcbfbef8680eb5749561f2918a57e1454486c76e390c1099c3f8ad5c10dc67f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Register User ";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        echo " <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>
    <script> 
        \$(document).ready(function(){
            \$('input[name=email]').keyup(function(){
               \$('#result').load('/emailexists/'+\$(this).val());
            });
        });
    </script>
    ";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "    <h1>Register User</h1>
";
        // line 15
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 16
            echo "    <ul>
        ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 18
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "   
    </ul>
";
        }
        // line 22
        echo "    <form method=\"POST\">
    Name: <input type=\"text\" name=\"name\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "name", array()), "html", null, true);
        echo "\"><br><br>
    Email: <input type=\"email\" name=\"email\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "email", array()), "html", null, true);
        echo "\"><span id=\"result\"></span><br><br>
    Password: <input type=\"password\" name=\"pass1\"><br><br>
    Confirm Password: <input type=\"password\" name=\"pass2\"><br><br>
    <input type =\"submit\" value=\"Register\"> 
</form>  
";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 24,  82 => 23,  79 => 22,  74 => 19,  65 => 18,  61 => 17,  58 => 16,  56 => 15,  53 => 14,  50 => 13,  36 => 4,  30 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"master.html.twig\" %}

{% block title %} Register User {% endblock %}
{% block head %} <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>
    <script> 
        \$(document).ready(function(){
            \$('input[name=email]').keyup(function(){
               \$('#result').load('/emailexists/'+\$(this).val());
            });
        });
    </script>
    {% endblock %}
{% block content %}
    <h1>Register User</h1>
{% if errorList %}
    <ul>
        {% for error in errorList %}
            <li>{{error}}</li>
        {% endfor %}   
    </ul>
{% endif %}
    <form method=\"POST\">
    Name: <input type=\"text\" name=\"name\" value=\"{{v.name}}\"><br><br>
    Email: <input type=\"email\" name=\"email\" value=\"{{v.email}}\"><span id=\"result\"></span><br><br>
    Password: <input type=\"password\" name=\"pass1\"><br><br>
    Confirm Password: <input type=\"password\" name=\"pass2\"><br><br>
    <input type =\"submit\" value=\"Register\"> 
</form>  
{% endblock %}
";
    }
}
