<?php

/* index.html.twig */
class __TwigTemplate_1186c061694f516e7af4a7485c8b569594e72e19577623afcaa9a52c61375f47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " E-Shop";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <h1>Welcome to e-shop</h1>
    ";
        // line 7
        if ((isset($context["user"]) ? $context["user"] : null)) {
            // line 8
            echo "        <p>Hello ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "name", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array()), "html", null, true);
            echo "). </p>
        <p>You may <a href=\"/logout\">Logout</a></p>
        ";
        } else {
            // line 11
            echo "        <p>You may <a href=\"/login\">Login</a> or <a href=\"/register\">Register</a></p>    
    ";
        }
        // line 13
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["productList"]) ? $context["productList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 14
            echo "        <hr>
        <h3>";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "</h3>
        <img src=\"/images/";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "imagePath", array()), "html", null, true);
            echo "\">
        <p>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "description", array()), "html", null, true);
            echo "</p>
        <p>Price per unit ";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price", array()), "html", null, true);
            echo "</p>

    <form method=\"POST\" action=\"/cart\">
        <input type=\"hidden\" name=\"productID\" value=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "ID", array()), "html", null, true);
            echo "\">
        Ad to cart <input type=\"number\"value=\"1\" style=\"width:30px;\" name=\"quantity\">
        <input type=\"submit\" value=\"Add o cart\">
                          
    </form>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 21,  76 => 18,  72 => 17,  68 => 16,  64 => 15,  61 => 14,  56 => 13,  52 => 11,  43 => 8,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"master.html.twig\" %}

{% block title %} E-Shop{% endblock %}

{% block content %}
    <h1>Welcome to e-shop</h1>
    {% if user %}
        <p>Hello {{user.name}} ({{user.email}}). </p>
        <p>You may <a href=\"/logout\">Logout</a></p>
        {% else %}
        <p>You may <a href=\"/login\">Login</a> or <a href=\"/register\">Register</a></p>    
    {% endif %}
    {% for product in productList %}
        <hr>
        <h3>{{product.name}}</h3>
        <img src=\"/images/{{product.imagePath}}\">
        <p>{{product.description}}</p>
        <p>Price per unit {{product.price}}</p>

    <form method=\"POST\" action=\"/cart\">
        <input type=\"hidden\" name=\"productID\" value=\"{{product.ID}}\">
        Ad to cart <input type=\"number\"value=\"1\" style=\"width:30px;\" name=\"quantity\">
        <input type=\"submit\" value=\"Add o cart\">
                          
    </form>
            {% endfor %}
{% endblock %}";
    }
}
