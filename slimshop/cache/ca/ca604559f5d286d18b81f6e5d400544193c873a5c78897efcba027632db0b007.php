<?php

/* logout_success.html.twig */
class __TwigTemplate_30009c44f51cb32578c0b7358928691004800fa0e99514c1f263f7fe38ca8acb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<p>You are logged out<a href=\"/\">Click to continue</a></p>";
    }

    public function getTemplateName()
    {
        return "logout_success.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 2,);
    }

    public function getSource()
    {
        return "{# empty Twig template #}
<p>You are logged out<a href=\"/\">Click to continue</a></p>";
    }
}
