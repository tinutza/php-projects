<?php

/* book.html.twig */
class __TwigTemplate_37b4c0937d69d96d9e03ebd20b39d2ed6d38d9cfb939c48014ee1010d72c1e6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "book.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Book Travel";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        if ((isset($context["user"]) ? $context["user"] : null)) {
            // line 7
            echo "        <h1>Hello ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "name", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "passport", array()), "html", null, true);
            echo ")</h1>
        <h1>Book a Travel</h1>
        ";
            // line 9
            if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
                // line 10
                echo "            <ul>
                ";
                // line 11
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                    // line 12
                    echo "                    <li>";
                    echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                    echo "</li>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 13
                echo "   
            </ul>
        ";
            }
            // line 16
            echo "        <p>You want to travel</p>
        <form method=\"post\">
            <label>From:</label> <input type=\"text\" name=\"fromAirport\" value=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "fromAirport", array()), "html", null, true);
            echo "\"><br><br>
            <label>To: </label> <input type=\"text\" name=\"toAirport\" value=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "toAirport", array()), "html", null, true);
            echo "\"><br><br>
            <input type=\"submit\" value=\"Book Now\">
        </form>


    ";
        } else {
            // line 25
            echo "        <p>Only registered users can book a travel. Please <a href=\"/login\">Login</a> or <a href=\"/register\">Register</a> first</p>    
    ";
        }
    }

    public function getTemplateName()
    {
        return "book.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 25,  80 => 19,  76 => 18,  72 => 16,  67 => 13,  58 => 12,  54 => 11,  51 => 10,  49 => 9,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"master.html.twig\" %}

{% block title %} Book Travel{% endblock %}

{% block content %}
    {% if user %}
        <h1>Hello {{user.name}} ({{user.passport}})</h1>
        <h1>Book a Travel</h1>
        {% if errorList %}
            <ul>
                {% for error in errorList %}
                    <li>{{error}}</li>
                    {% endfor %}   
            </ul>
        {% endif %}
        <p>You want to travel</p>
        <form method=\"post\">
            <label>From:</label> <input type=\"text\" name=\"fromAirport\" value=\"{{v.fromAirport}}\"><br><br>
            <label>To: </label> <input type=\"text\" name=\"toAirport\" value=\"{{v.toAirport}}\"><br><br>
            <input type=\"submit\" value=\"Book Now\">
        </form>


    {% else %}
        <p>Only registered users can book a travel. Please <a href=\"/login\">Login</a> or <a href=\"/register\">Register</a> first</p>    
    {% endif %}
{% endblock %}";
    }
}
