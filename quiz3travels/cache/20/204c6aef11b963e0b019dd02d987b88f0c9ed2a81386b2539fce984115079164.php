<?php

/* master.html.twig */
class __TwigTemplate_270b6627f11c2fce84c223c16a5a1c84bfc0e3827fcceeca63e3533050e860b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
       
            <link rel=\"stylesheet\" href=\"/styles.css\" />
            <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 7
        $this->displayBlock('head', $context, $blocks);
        // line 9
        echo "    </head>
    <body>
        <div id=\"centerContent\">
        <div id=\"content\">";
        // line 12
        $this->displayBlock('content', $context, $blocks);
        echo "</div>
        <div id=\"footer\">
                &copy; Copyright 2016 by IPD8
        </div>
        </div>
    </body>
</html>";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
    }

    // line 7
    public function block_head($context, array $blocks = array())
    {
        // line 8
        echo "        ";
    }

    // line 12
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  63 => 12,  59 => 8,  56 => 7,  51 => 6,  40 => 12,  35 => 9,  33 => 7,  29 => 6,  22 => 1,);
    }

    public function getSource()
    {
        return "<!DOCTYPE html>
<html>
    <head>
       
            <link rel=\"stylesheet\" href=\"/styles.css\" />
            <title>{% block title %}{% endblock %}</title>
        {% block head %}
        {% endblock %}
    </head>
    <body>
        <div id=\"centerContent\">
        <div id=\"content\">{% block content %}{% endblock %}</div>
        <div id=\"footer\">
                &copy; Copyright 2016 by IPD8
        </div>
        </div>
    </body>
</html>";
    }
}
