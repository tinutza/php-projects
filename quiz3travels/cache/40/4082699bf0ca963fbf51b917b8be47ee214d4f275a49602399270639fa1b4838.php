<?php

/* index.html.twig */
class __TwigTemplate_2d53997c3103f06001bb8b7eeacfd73692767fc068df0986f9135907abedb93b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Travels";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        if ((isset($context["user"]) ? $context["user"] : null)) {
            // line 7
            echo "        <p>Hello ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "name", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "passport", array()), "html", null, true);
            echo "). </p>
        <p>You may <a href=\"/book\">Book</a> or <a href=\"/logout\">Logout</a></p>
        ";
            // line 9
            if ((isset($context["bookingList"]) ? $context["bookingList"] : null)) {
                // line 10
                echo "            <p> Here are your bookings </p>
            <table>
                <tr>
                    <th>Booking ID</th>
                    <th>Departure Airport</th>
                    <th>Arrival Airport</th>
                </tr>

                ";
                // line 18
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["bookingList"]) ? $context["bookingList"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["booking"]) {
                    // line 19
                    echo "                    <tr>
                        <td>";
                    // line 20
                    echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "ID", array()), "html", null, true);
                    echo "</td><td>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "fromAirport", array()), "html", null, true);
                    echo "</td><td>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "toAirport", array()), "html", null, true);
                    echo "</td>
                    </tr>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['booking'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 23
                echo "
            </table>
        ";
            } else {
                // line 26
                echo "            <p>There are currently no bookings to view</p>  
        ";
            }
            // line 28
            echo "        ";
        } else {
            // line 29
            echo "        <p>You are not logged in yet. You may <a href=\"/login\">Login</a> or <a href=\"/register\">Register</a></p>    
    ";
        }
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 29,  90 => 28,  86 => 26,  81 => 23,  68 => 20,  65 => 19,  61 => 18,  51 => 10,  49 => 9,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"master.html.twig\" %}

{% block title %} Travels{% endblock %}

{% block content %}
    {% if user %}
        <p>Hello {{user.name}} ({{user.passport}}). </p>
        <p>You may <a href=\"/book\">Book</a> or <a href=\"/logout\">Logout</a></p>
        {% if bookingList %}
            <p> Here are your bookings </p>
            <table>
                <tr>
                    <th>Booking ID</th>
                    <th>Departure Airport</th>
                    <th>Arrival Airport</th>
                </tr>

                {%for booking in bookingList %}
                    <tr>
                        <td>{{booking.ID}}</td><td>{{booking.fromAirport}}</td><td>{{booking.toAirport}}</td>
                    </tr>
                {% endfor %}

            </table>
        {% else %}
            <p>There are currently no bookings to view</p>  
        {% endif %}
        {% else %}
        <p>You are not logged in yet. You may <a href=\"/login\">Login</a> or <a href=\"/register\">Register</a></p>    
    {% endif %}
{% endblock %}";
    }
}
