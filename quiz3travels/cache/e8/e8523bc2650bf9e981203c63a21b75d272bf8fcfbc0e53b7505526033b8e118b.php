<?php

/* register.html.twig */
class __TwigTemplate_2c4ab525631a3697f8123032c14da10cc7dd5b1467ac8bc280b911e5fcf13121 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Register User ";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <h1>Register User</h1>
";
        // line 7
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 8
            echo "    <ul>
        ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 10
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 11
            echo "   
    </ul>
";
        }
        // line 14
        echo "    <form method=\"POST\">
        <label> Name:</label> <input type=\"text\" name=\"name\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "name", array()), "html", null, true);
        echo "\"><br><br>
        <label> Passport:</label> <input type=\"text\" name=\"passport\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "passport", array()), "html", null, true);
        echo "\"><br><br>
        <label>Password: </label> <input type=\"password\" name=\"pass1\"><br><br>
        <label>Confirm Password:</label> <input type=\"password\" name=\"pass2\"><br><br>
    <input type =\"submit\" value=\"Register\"> 
</form>  
";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 16,  67 => 15,  64 => 14,  59 => 11,  50 => 10,  46 => 9,  43 => 8,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"master.html.twig\" %}

{% block title %} Register User {% endblock %}

{% block content %}
    <h1>Register User</h1>
{% if errorList %}
    <ul>
        {% for error in errorList %}
            <li>{{error}}</li>
        {% endfor %}   
    </ul>
{% endif %}
    <form method=\"POST\">
        <label> Name:</label> <input type=\"text\" name=\"name\" value=\"{{v.name}}\"><br><br>
        <label> Passport:</label> <input type=\"text\" name=\"passport\" value=\"{{v.passport}}\"><br><br>
        <label>Password: </label> <input type=\"password\" name=\"pass1\"><br><br>
        <label>Confirm Password:</label> <input type=\"password\" name=\"pass2\"><br><br>
    <input type =\"submit\" value=\"Register\"> 
</form>  
{% endblock %}
";
    }
}
