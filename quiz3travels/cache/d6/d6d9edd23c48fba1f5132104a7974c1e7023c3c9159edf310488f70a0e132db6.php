<?php

/* book_success.html.twig */
class __TwigTemplate_2798d021db0a2402ee8a1486500d43090f2942406604714fd52136266d6af610 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "book_success.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Booking Success ";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <p>You succesfully booked a travel from ";
        echo twig_escape_filter($this->env, (isset($context["fromAirport"]) ? $context["fromAirport"] : null), "html", null, true);
        echo " to ";
        echo twig_escape_filter($this->env, (isset($context["toAirport"]) ? $context["toAirport"] : null), "html", null, true);
        echo ".</p>
    <a href=\"/\">Click to continue</a>
";
    }

    public function getTemplateName()
    {
        return "book_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"master.html.twig\" %}

{% block title %} Booking Success {% endblock %}

{% block content %}
    <p>You succesfully booked a travel from {{fromAirport}} to {{toAirport}}.</p>
    <a href=\"/\">Click to continue</a>
{% endblock %}
";
    }
}
