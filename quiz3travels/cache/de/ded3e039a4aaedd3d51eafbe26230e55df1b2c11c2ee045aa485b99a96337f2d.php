<?php

/* error_internal.html.twig */
class __TwigTemplate_7cfc6ba52c0cd2cad7e66c72ea5ce339c7c54f8a80d844990c4805fe00533b8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "error_internal.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Internal Error ";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "<h1>500 - Internal error</h1>

<p>Our apologies. We've encountered an internal error in our system.
    Our technical team has been notified.</p>

<a href=\"/\">Click here to continue</a>
";
    }

    public function getTemplateName()
    {
        return "error_internal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"master.html.twig\" %}

{% block title %} Internal Error {% endblock %}

{% block content %}
<h1>500 - Internal error</h1>

<p>Our apologies. We've encountered an internal error in our system.
    Our technical team has been notified.</p>

<a href=\"/\">Click here to continue</a>
{% endblock %}";
    }
}
