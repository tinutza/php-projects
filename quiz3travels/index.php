<?php

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

session_start();

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/erros.log', Logger::ERROR));

DB::$dbName = 'quiz3travels';
DB::$user = 'quiz3travels';
DB::$password = '2WEfvBZCmAS4w9yZ';
DB::$error_handler = 'sql_error_handler';
DB::$nonsql_error_handler = 'nonsql_error_handler';

function nonsql_error_handler($params) {
    global $app, $log;
    $log->error('Database Error: ' . $params['error']);
    http_response_code(500);
    $app->render('error_internal.html.twig');
    die;
}

function sql_error_handler($params) {
    global $app, $log;
    $log->error('Sql Error: ' . $params['error']);
    $log->error(' in query: ' . $params['query']);
    http_response_code(500);
    $app->render('error_internal.html.twig');
    die; // don't want to keep going if a query broke
}

// instantiate Slim - router in front controller (this file)
// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');


if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = array();
}

$app->get('/', function() use ($app) {
    $bookingList = array();
    if($_SESSION['user']){
     $bookingList = DB::query('SELECT * FROM booking WHERE passengerID=%s', $_SESSION['user']['ID']);
    }
    $app->render('index.html.twig', array(
        'bookingList' => $bookingList,
        'user' => $_SESSION['user']));
});


$app->get('/register', function() use ($app) {
    $app->render("register.html.twig");
});

//Submission received (state 2 or 3)
$app->post('/register', function() use ($app, $log) {
    $name = $app->request->post('name');
    $passport = $app->request->post('passport');
    $pass1 = $app->request->post('pass1');
    $pass2 = $app->request->post('pass2');

    $valueList = array('name' => $name, 'passport' => $passport);

    $errorList = array();
    if (strlen($name) < 4) {
        array_push($errorList, "Name must be at least 4 characters long");
        unset($valueList['name']);
    }
    if (!preg_match('/^[A-Z]{2}[0-9]{6}$/', $passport)) {
        array_push($errorList, "Passport must be in format AA012345");
        unset($valueList['passport']);
    } else {
        if (DB::queryOneRow("SELECT ID FROM passenger WHERE passport=%s", $passport)) {
            array_push($errorList, "User with this passport number is already registered");
            unset($valueList['passport']);
        }
    }
    if (!preg_match('/[A-Z]/', $pass1) ||
            !preg_match('/[a-z]/', $pass1) || !preg_match('/[0-9$@$!%*#?&]/', $pass1) || strlen($pass1) < 8) {
        array_push($errorList, "The password must be at leat 8 characters long and contain at least one upper case, one lower case, one digit or one special characters ");
    } else if ($pass2 != $pass1) {
        array_push($errorList, "The two passwords must be exactly the same");
    }
    if ($errorList) {
        //State3: failed submition
        $app->render('register.html.twig', array(
            'errorList' => $errorList,
            'v' => $valueList
        ));
    } else {
        //State2: successful submition
        DB::insert('passenger', array(
            'name' => $name,
            'passport' => $passport,
            'password' => $pass1
        ));
        $id = DB::insertID();
        $log->debug("Registered user with ID=" . $id);
        $app->render('register_success.html.twig');
    }
});

$app->get('/login', function() use ($app) {
    $app->render("login.html.twig");
});

//Submission received (state 2 or 3)
$app->post('/login', function() use ($app, $log) {
    $passport = $app->request->post('passport');
    $password = $app->request->post('password');

    $user = DB::queryFirstRow("SELECT * FROM passenger WHERE passport=%s", $passport);

    if ($user && $user['password'] === $password) {

        unset($user['password']);
        $_SESSION['user'] = $user;
        // print_r($_SESSION['user']) ;

        $log->debug(sprintf("User for passport %s from IP %s succsefully logged in", $user['passport'], $_SERVER['REMOTE_ADDR']));
        $app->render('login_success.html.twig');
    } else {
        $log->debug(sprintf("User failed for passport %s from IP %s", $user['passport'], $_SERVER['REMOTE_ADDR']));
        $app->render('login.html.twig', array(
            'loginFailed' => TRUE));
    }
});

$app->get('/logout', function() use ($app) {
    $_SESSION['user'] = array();
    $app->render("logout_success.html.twig");
});

$app->get('/book', function() use ($app) {
    $app->render('book.html.twig', array(
        'user' => $_SESSION['user']));
});

$app->post('/book', function() use ($app, $log) {
    $fromAirport = $app->request->post('fromAirport');
    $toAirport = $app->request->post('toAirport');

    $valueList = array('fromAirport' => $fromAirport, 'toAirport' => $toAirport);

    $errorList = array();
    
    if (!preg_match('/^[A-Z]{3}$/', $fromAirport)) {
        array_push($errorList, "Departure Airport must consist of 3 uppercase letters");
        unset($valueList['fromAirport']);
    } 
    if (!preg_match('/^[A-Z]{3}$/', $toAirport)) {
        array_push($errorList, "Arrival Airport must consist of 3 uppercase letters");
        unset($valueList['toAirport']);
    } elseif ($fromAirport == $toAirport) {
        array_push($errorList, "The Departure Airport and the Arrival Airport cannot be the same");
    }
    
    if ($errorList) {
        //State3: failed submition
        $app->render('book.html.twig', array(
            'errorList' => $errorList,
            'v' => $valueList,
            'user' => $_SESSION['user']
        ));
    } else {
        //State2: successful submition
        DB::insert('booking', array(
            'passengerID' => $_SESSION['user']['ID'],
            'fromAirport' => $fromAirport,
            'toAirport' => $toAirport
        ));
        $id = DB::insertID();
        $log->debug(sprintf("Travel %d booked", $id));
        $app->render('book_success.html.twig', array(
            'fromAirport' => $fromAirport,
            'toAirport'=>$toAirport
        ));
    }
});
$app->run();
