<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <a href="studentadd.php">Add New Student</a>
        <table border="1">
            <tr>
                <th>#</th>
                <th>Name</th>
            </tr>
            <?php
            require_once 'db.php';

            $sql = "SELECT * FROM student";

            $result = mysqli_query($conn, $sql);
            if (!$result) {
                echo "Error executing query [ $sql ] : " . mysqli_error($conn);
                exit;
            }
            $dataRows = mysqli_fetch_all($result, MYSQLI_ASSOC);

            foreach ($dataRows as $row) {
                $ID = $row['ID'];
                $name = htmlspecialchars($row['name']);

                echo "<tr><td>$ID</td>";
                echo "<td><a href=\"studentview.php?id=$ID\">$name</a></td>";
                echo "</tr>\n";
            }
            ?>
        </table>
    </body>
</html>
