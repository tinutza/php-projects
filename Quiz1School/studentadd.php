<h3>Add a New Student</h3>

<?php

function getForm($nn = '', $aa = '', $gg = '', $hh = '') {
    //Set the isDone checkbox checked if the value for $hh is "yes"
    if ($hh == "yes") {
        $checked = "checked";
    } else {
        $checked = "";
    }
    //heredoc
    $form = <<< ENDTAG
     <form method="POST">
    Student Name: <input type="text" name="name" value="$nn"><br><br>
    Age: <input type="number" name="age" value="$aa"><br><br>
    GPA: <input type="text" name="gpa" value="$gg"><br><br>
    Has Graduated: <input type="checkbox" name="hasGraduated" value="yes" {$checked}><br><br>
    
    <input type ="submit" value="Add"> 
</form>  
ENDTAG;
    return $form;
}

if (!isset($_POST['name'])) {
    //First Show if no data is provided
    echo getForm();
} else {
    //Receiving a submission
    $name = $_POST['name'];
    $age = $_POST['age'];
    $gpa = $_POST['gpa'];
    //If checkboc is checked than assign value "yes" to $hasGraduated, else assign "no"
    if (isset($_POST['hasGraduated'])) {
        $hasGraduated = $_POST['hasGraduated'];
    } else {
        $hasGraduated = "no";
    }

    //Validate input 
    $errorList = array();
    //Check if name is at least 4 characters long
    if (strlen($name) < 4) {
        array_push($errorList, "Name must be at least 4 characters long");
    }
    //Check if age is an integer and between 0 and 150
    if (!is_numeric($age) || $age < 1 || $age > 150 ) {
        array_push($errorList, "Age must be a valid integer between 1 and 150");
    }
    //allow empty gpa or if it is provided it should be numeric and between 0 and 4.3
    if (!empty($gpa) && ($gpa < 0 || $gpa > 4.3 || !is_numeric($gpa))) {
        array_push($errorList, "Gpa must be empty or a valid decimal number between 0 and 4.3");
    }

    //Display error messages if invalid data is submitted
    if ($errorList) {
        //submission failed
        echo "<h5>Problems  found in your submission</h5>\n";
        echo "</ul>\n";
        foreach ($errorList as $error) {
            echo "<li>" . htmlspecialchars($error) . "</li>";
        }
        echo "</ul><br><br><br><hr>";
        echo getForm($name, $age, $gpa, $hasGraduated);
    } else {
        //submition succesfull
        require_once 'db.php';

        $sql = sprintf("INSERT INTO student VALUES (NULL, '%s', '%s', '%s', '%s')", mysqli_escape_string($conn, $name), mysqli_escape_string($conn, $age), mysqli_escape_string($conn, $gpa), mysqli_escape_string($conn, $hasGraduated));
        $result = mysqli_query($conn, $sql);
        if (!$result) {
            echo "Error executing query [$sql] : " . mysqli_error($conn);
        } else {

            echo "Submission Succesfull<br><br>\n";
            echo "<a href=\"index.php\">Go to Home Page</a>";
        }
    }
}
