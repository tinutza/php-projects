<?php

echo "<h3>Student Info</h3>";
echo "<ul>";
//Check if id is provided, if not display error message to user and exit script
if (!isset($_GET['id'])) {

    die("<p>No Student ID Provided </p><hr><a href=\"index.php\"> Back to home page</a> ");
}
 
$id = $_GET['id'];

require_once 'db.php';

$sql = sprintf("SELECT * FROM student where ID = '%s'", mysqli_escape_string($conn, $id));

$result = mysqli_query($conn, $sql);
//Check if the sql query executed succesfully
if (!$result) {
    die("Error. Could not fetch data from the database: " . mysqli_error($conn));
}
//Fetch one row from the database
$row = mysqli_fetch_assoc($result);

//If no record is found, display an appropriate message to the user
if ($row == false) {
    echo "Could not find the student with id: " . $id . " " . mysqli_error($conn);
} else {
    $ID = $row['ID'];
    $name = htmlspecialchars($row['name']);
    $age = $row['age'];
    $gpa = $row['gpa'];
    $hasGraduated = htmlspecialchars($row['hasGraduated']);
    echo "<li>#: $ID</li>";
    echo "<li>Name: $name</li>";
    echo "<li>Age: $age</li>";
    echo "<li>GPA: $gpa</li>";
    if ($hasGraduated == "yes") {
        echo "<li>Has Graduated</li>";
    } else {
        echo "<li>Not Graduated</li>";
    }
    echo "</li>\n";
    echo "</ul>\n";
}
?>
<hr>
<a href="index.php"> Back to home page</a>


