<?php

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/erros.log', Logger::ERROR));




DB::$dbName = 'slimads';
DB::$user = 'slimads';
DB::$password = '69tfKqNcFP4XAzx3';
DB::$error_handler = 'sql_error_handler';
DB::$nonsql_error_handler = 'nonsql_error_handler';

function nonsql_error_handler($params) {
    global $app, $log;
    $log->error('Database Error: '. $params['error']);
    http_response_code(500);
    $app->render('error_internal.html.twig');
    die;
}

function sql_error_handler($params) {
    global $app, $log;
    $log->error('Sql Error: '. $params['error']);
    $log->error(' in query: '. $params['query']);
    http_response_code(500);
    $app->render('error_internal.html.twig');
    die; // don't want to keep going if a query broke
}

// instantiate Slim - router in front controller (this file)
// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

\Slim\Route::setDefaultConditions(array('id' => '\d+'));

$app->get('/', function() use ($app) {
    $adList = DB::query('SELECT * FROM ad');
    $app->render('index.html.twig', array('adList' => $adList));
});

//STATE 1 FIRST SHOW
//
//
//$app->get('/postad', function() use ($app){
//    $app->render('postad.html.twig');
//});
$app->get('/postad(/:id)', function($id = '') use ($app) {
    if ($id == '') {
        $app->render("postad.html.twig");
        return;
    }
    $ad = DB::queryOneRow("SELECT * FROM ad WHERE ID=%d", $id);
    if (!$ad) {
        $app->render("editad_notfound.html.twig");
    } else {
        $app->render("postad.html.twig", array("v" => $ad));
    }
});
//Submission received (state 2 or 3)
$app->post('/postad(/:id)', function($id = '') use ($app, $log) {
    $msg = $app->request->post('msg');
    $price = $app->request->post('price');
    $email = $app->request->post('contactEmail');

    $valueList = array('msg' => $msg, 'price' => $price, 'contactEmail' => $email);

    $errorList = array();
    if (strlen($msg) < 5 || strlen($msg) > 300) {
        array_push($errorList, " Message must be at least 5 and at most 300 characters long ");
        //unset($valueList['msg']);
    }
    if ($price == "" || !is_numeric($price) || $price < 0 || $price > 1000000) {
        array_push($errorList, " Price must be provided and it must be a decimal value between 0 and 1000000 ");
        unset($valueList['price']);
    }
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
        array_push($errorList, "It doesn't look like a valid email");
        unset($valueList['email']);
    }
    if ($errorList) {
        //State3: failed submition
        $app->render('postad.html.twig', array(
            'errorList' => $errorList,
            'v' => $valueList
        ));
    } else {
        //State2: successful submition
        if ($id == "") {
            DB::insert('ad', array(
                'msg' => $msg,
                'price' => $price,
                'contactEmail' => $email
            ));
            $id = DB::insertID();
            $log->debug("Ad created with ID=" . $id);
        } else {
            DB::update('ad', array(
                'msg' => $msg,
                'price' => $price,
                'contactEmail' => $email
                    ), 'ID=%d', $id);
            $log->debug("Ad updated with ID=" . $id);
        }

        $app->render('postad_success.html.twig');
    }
});




$app->run();
