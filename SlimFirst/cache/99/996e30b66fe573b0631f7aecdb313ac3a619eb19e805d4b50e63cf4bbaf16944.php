<?php

/* sayhello.html.twig */
class __TwigTemplate_4f338f2e89fe4e1ef85b081858577cc4586e6531f1481095289d6488bf28ab8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<h1>Say hello</h1>
";
        // line 3
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 4
            echo "    <ul>
        ";
            // line 5
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 6
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 7
            echo "   
    </ul>
";
        }
        // line 10
        echo "<form method=\"post\">
    Name: <input type=\"text\" name=\"name\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "name", array()), "html", null, true);
        echo "\"><br>
    Age: <input type=\"number\" name=\"age\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "age", array()), "html", null, true);
        echo "\"><br>
    <input type=\"submit\" value=\"Say hello\">
</form>";
    }

    public function getTemplateName()
    {
        return "sayhello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 12,  48 => 11,  45 => 10,  40 => 7,  31 => 6,  27 => 5,  24 => 4,  22 => 3,  19 => 2,);
    }

    public function getSource()
    {
        return "{# empty Twig template #}
<h1>Say hello</h1>
{% if errorList %}
    <ul>
        {% for error in errorList %}
            <li>{{error}}</li>
        {% endfor %}   
    </ul>
{% endif %}
<form method=\"post\">
    Name: <input type=\"text\" name=\"name\" value=\"{{v.name}}\"><br>
    Age: <input type=\"number\" name=\"age\" value=\"{{v.age}}\"><br>
    <input type=\"submit\" value=\"Say hello\">
</form>";
    }
}
