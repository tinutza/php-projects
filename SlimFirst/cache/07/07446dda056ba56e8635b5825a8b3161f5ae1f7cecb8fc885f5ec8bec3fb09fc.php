<?php

/* sayhello_success.html.twig */
class __TwigTemplate_d31e35dace3da5f27ba4f75fd093656d08a4742559040063e2837fbda60cfd11 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<p>Hello ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo ", you are ";
        echo twig_escape_filter($this->env, (isset($context["age"]) ? $context["age"] : null), "html", null, true);
        echo " years old.</p>";
    }

    public function getTemplateName()
    {
        return "sayhello_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 2,);
    }

    public function getSource()
    {
        return "{# empty Twig template #}
<p>Hello {{name}}, you are {{age}} years old.</p>";
    }
}
