<h3>Registration Form</h3>

<?php
require_once 'db.php';
function getForm($nn = '', $ee = '') {
    $form = <<< ENDTAG
    <form method="POST">
    Name: <input type="text" name="name" value="$nn"><br><br>
    Email: <input type="email" name="email" value="$ee"><br><br>
    Password: <input type="password" name="password1" value=""><br><br>
    Confirm Password: <input type="password" name="password2" value=""><br><br>
    <input type ="submit" value="Register"> 
</form>  
ENDTAG;
    return $form;
}

if (!isset($_POST['name'])) {
    //First Show if no data is provided
    echo getForm();
} else {
    //Receiving a submission
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password1 = $_POST['password1'];
    $password2 = $_POST['password2'];

    //Validate input 
    $errorList = array();
    //Check if name is at least 4 characters long
    if (strlen($name) < 4) {
        array_push($errorList, "Name must be at least 4 characters long");
    }
    //Check if email input looks like a valid emai an if there is no such email already registered
    if($_POST['email']){
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE ) {
        array_push($errorList, "It doesn't look like a valid email");
    } 
    $sql = sprintf("SELECT email FROM users where email = '%s'", mysqli_escape_string($conn, $email));

            $result = mysqli_query($conn, $sql);
            if (!$result) {
                echo "Error executing query [ $sql ] : " . mysqli_error($conn);
            }
            if(mysqli_num_rows($result) !=0 ){
                array_push($errorList, "The email is already regstered. Please login with that account or register with a different one ");
            }
}
    //Check if password one has at least one capital letter, one lower case, one digit ad one special charater ad at least 8
    if (!preg_match('/[A-Z]/', $password1) || 
            !preg_match('/[a-z]/', $password1) || !preg_match('/[0-9$@$!%*#?&]/', $password1)
                  || strlen($password1)<8) {
        array_push($errorList, "The password must be at leat 8 characters long and contain at least one upper case, one lower case, one digit or one special characters ");
    } else if ($password2 != $password1)  {
        array_push($errorList, "The two passwords must be exactly the same");
    }
    //Display error messages if invalid data is submitted
    if ($errorList) {
        //submission failed
        echo "<h5>Problems  found in your submission</h5>\n";
        echo "</ul>\n";
        foreach ($errorList as $error) {
            echo "<li>" . htmlspecialchars($error) . "</li>";
        }
        echo "</ul><br><br><br><hr>";
        echo getForm($name, $email);
    } else {
        //submition succesfull
        $sql = sprintf("INSERT INTO users VALUES (NULL, '%s', '%s', '%s')", mysqli_escape_string($conn, $name), mysqli_escape_string($conn, $email), mysqli_escape_string($conn, $password1));
        $result = mysqli_query($conn, $sql);
        if (!$result) {
            echo "Error executing query [$sql] : " . mysqli_error($conn);
        } else {

            echo "Registration Succesful<br><br>\n";
            echo "<a href=\"index.php\">Go to Login page</a>";
        }
    }
}
