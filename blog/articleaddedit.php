<h3>Add New Article</h3>

<?php
require_once 'db.php';

if (!isset($_SESSION['user'])) {
    die("You must log in first if you want to post an article");
}

function getForm($subj = '', $bb = '') {
    $form = <<< ENDTAG
    <form method="POST" enctype="multipart/form-data">
    Subject: <input type="text" name="subject" value="$subj"><br><br>
    Body: <textarea  rows="20" cols="50" name="body">$bb</textarea><br><br>
    <input type="file" name="fileToUpload">
    <input type ="submit" value="Save Article" name="submit"> 
</form>  
ENDTAG;
    return $form;
}
$isEdit = FALSE;
$sql = "";

if(isset($_GET['id'])){
    $isEdit = TRUE;
}

if (!isset($_POST['submit'])) {
    //First Show
    if ($isEdit) {
        
        $articleID = $_GET['id'];
        
        $sql = sprintf("SELECT title, body FROM articles WHERE ID='%s'", mysqli_escape_string($conn, $articleID));

        $result = mysqli_query($conn, $sql);
        if (!$result) {
            die("Error executing query [ $sql ] : " . mysqli_error($conn));
        }
        $row = mysqli_fetch_assoc($result);
        if (!$row) {
            die("Could not find article with id: " . $articleID);
        }
        $oldTitle = htmlspecialchars($row['title']);
        $oldBody = htmlspecialchars($row['body']);
        echo getForm($oldTitle, $oldBody);
    } else {
        echo getForm();
    }
} else {
    //Receiving a submission
    $subject = $_POST['subject'];
    $body = $_POST['body'];
    $target_dir = "uploads/";
    $max_file_size = 5 * 1024 * 1024; // 5000000
//echo "<pre>\n";
//echo "\$_FILES:\n";
//print_r($_FILES);
//Validate input 
    $errorList = array();
    //Check if subject is at least 4 characters long
    if (strlen($subject) < 4) {
        array_push($errorList, "Subject must be at least 4 characters long");
    }
    if (strlen($body) < 50) {
        array_push($errorList, "Body must be at least 50 characters long");
    }
   if (!$isEdit && empty($_FILES['fileToUpload']['name'])) {
        array_push($errorList, "You must choose an image to upload");
   } 
   if(!empty($_FILES['fileToUpload']['name'])){
        $fileUpload = $_FILES['fileToUpload'];

        $check = getimagesize($fileUpload['tmp_name']);
        if (!$check) {
            array_push($errorList, "Error: File upload was not an image file.");
        } elseif (!in_array($check['mime'], array('image/png', 'image/gif', 'image/bmp', 'image/jpeg'))) {
            array_push($errorList, "Error: Only accepting value png,gif,bmp,jpg files.");
        } elseif ($fileUpload['size'] > $max_file_size) {
            array_push($errorList, "Error: File to big, maximuma accepted is $max_file_size bytes");
        }
        
    } 
    
        //Display error messages if invalid data is submitted
        if ($errorList) {
            //submission failed
            echo "<h5>Problems  found in your submission</h5>\n";
            echo "</ul>\n";
            foreach ($errorList as $error) {
                echo "<li>" . htmlspecialchars($error) . "</li>";
            }
            echo "</ul><br><br><br><hr>";
            echo getForm($subject, $body);
        } else {
            //submition succesfull
            // WARNING (CYA): Make sure no '..' is allowed in the path
// SOLUTION 1: use the original file name
//            if (strstr($fileUpload['name'], '..')) {
//                array_push($errorList, "Error: do not mess with the Zohan");
//            }
//            $target_file = $target_dir . $fileUpload['name'];
// SOLUTION 2: generate your own file name
            $target_file ="";
            if(!empty($_FILES['fileToUpload']['name'])){
            $file_extension = explode('/', $check['mime'])[1];
            $target_file = $target_dir . md5($fileUpload["name"] . time()) . '.' . $file_extension;
            
            if (move_uploaded_file($fileUpload["tmp_name"], $target_file)) {
                echo "The file " . basename($fileUpload["name"]) . " has been uploaded.";
            } else {
                die("Fatal eror.There was a server-side error on uploading your file.");
            }
            if($isEdit) {
                //If it is edit and an image is selected, update all the fields
                $sql = sprintf("UPDATE articles SET title = '%s', body  = '%s', imagePath='%s' WHERE ID = '%s'", 
                  mysqli_escape_string($conn, $subject),
                  mysqli_escape_string($conn, $body),
                  mysqli_escape_string($conn, $target_file),
                  mysqli_escape_string($conn, $_GET['id']));
            } else {
                //if it is not edit form insert all the values
                $sql = sprintf("INSERT INTO articles VALUES (NULL, '%s', '%s', '%s', '%s', '%s')", 
                mysqli_escape_string($conn, $_SESSION['user']['ID']), 
                mysqli_escape_string($conn, date("Y-m-d")),
                mysqli_escape_string($conn, $subject), 
                mysqli_escape_string($conn, $body), 
                mysqli_escape_string($conn, $target_file)); 
            }
            } else {
                // If it is edit form and no image selected keep the old one in the database
                 $sql = sprintf("UPDATE articles SET title = '%s', body  = '%s' WHERE ID = '%s'", 
                  mysqli_escape_string($conn, $subject),
                  mysqli_escape_string($conn, $body),
                  mysqli_escape_string($conn, $_GET['id']));
            }
           
            $result = mysqli_query($conn, $sql);
            if (!$result) {
                echo "Error executing query [$sql] : " . mysqli_error($conn);
            } else {

                echo "The article was Saved succesfully<br><br>\n";
                echo "<a href=\"index.php\">Go to home page</a>";
            }
        }
    }
   