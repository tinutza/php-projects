<h3>Article View</h3>

<?php
require_once 'db.php';

function getCommentForm($bb = '') {
  return <<< ENDTAG
    <form method="POST">
    Body: <textarea  rows="4" cols="50" name="commentBody">$bb</textarea><br><br>
    <input type ="submit" value="Post Comment"> 
</form>  
ENDTAG;
}

if (!isset($_GET['id'])) {
    die('No article to view');
}

{
$sql = sprintf("SELECT * FROM articles, users WHERE authorID=users.ID AND articles.ID= '%s'", mysqli_escape_string($conn, $_GET['id']));
$result = mysqli_query($conn, $sql);

if (!$result) {
    die("Error executing query [$sql] : " . mysqli_error($conn));
}
$row = mysqli_fetch_assoc($result);
if (!$row) {
    die("No article was found");
}
$title = htmlspecialchars($row['title']);
$body = htmlspecialchars($row['body']);
$author = $row['name'];
$pubDate = $row['pubDate'];
$imagePath = htmlspecialchars($row['imagePath']);

echo "<h3>$title</h3>";
echo "<p><i>Posted by</i> $author at $pubDate</p><br><br><br>";
echo "<img src=\"$imagePath\" alt=\"Article Image\" style=\"width:304px;height:228px;\"><br><br><br>";
echo "<p>$body</p><br><br><hr><br><br>";
}

if(isset($_SESSION['user'])){
    echo "<p>Add a comment</p>";
if(!isset($_POST['commentBody'])){
        //Firt show
       echo getCommentForm();
    } else {
        //validate input
        $commentBody = $_POST['commentBody'];

        
        if(strlen($commentBody)<5){
            echo "The comment must be at least 5 characters long";
          echo getCommentForm($commentBody);
        } else {
            
            $sql = sprintf("INSERT INTO comments VALUES (NULL, '%s', '%s', '%s', '%s')",
                    mysqli_escape_string($conn, $_GET['id']),
                    mysqli_escape_string($conn, $_SESSION['user']['ID']), 
                    mysqli_escape_string($conn, $commentBody), 
                    mysqli_escape_string($conn, date('Y-m-d H:i:s')));
            
             $result = mysqli_query($conn, $sql);
             
            if (!$result) {
                echo "Error executing query [$sql] : " . mysqli_error($conn);
            } else {
                //Redirect to avoid duplicate comment entry or check before inserting if the comment already exists 
                 header('Location: articleview.php?id='.$_GET['id']);
                 exit();
            }
        }
    }
}else {
    echo "<p>To add a comment <a href=\"login.php\">Login</> or <a href=\"register.php\">Register</a> </p>";
}

//Display the last 5 comments;
{
$sql = sprintf("SELECT * FROM comments, users WHERE authorID=users.ID AND articleID= '%s' ORDER BY comments.ID desc LIMIT 5", mysqli_escape_string($conn, $_GET['id']));
$result = mysqli_query($conn, $sql);

if (!$result) {
    die("Error executing query [$sql] : " . mysqli_error($conn));
}
$dataRows = mysqli_fetch_all($result, MYSQLI_ASSOC);
if(!$dataRows){
    echo "No comments yet for this article";
}else {
        foreach ($dataRows as $row) {
            $author = htmlspecialchars($row['name']);
            $pubTimestamp = $row['pubTimestamp'];
            $body = htmlspecialchars($row['body']);

            echo "<p><i>$author said  on $pubTimestamp</i><p>";
            echo "<p>$body</p><br>";
        }
}
}


