<?php

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$dbName = 'quiz4board';
DB::$user = 'quiz4board';
DB::$password = 'UYTY6p8x7xDKFyhw';
DB::$error_handler = 'sql_error_handler';
DB::$nonsql_error_handler = 'nonsql_error_handler';

// FIXME: add monolog

function nonsql_error_handler($params) {
    global $app, $log;
    $log->error("Database error: " . $params['error']);
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode("Internal server error");
    die;
}

function sql_error_handler($params) {
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error(" in query: " . $params['query']);
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode("Internal server error");
    die; // don't want to keep going if a query broke
}

$app = new \Slim\Slim();

\Slim\Route::setDefaultConditions(array(
    'ID' => '\d+'
));

$app->response->headers->set('content-type', 'application/json');

    function isMessageValid($message, &$error) {
        //Check if the passed number of fields is exactly 3
        if(count($message) != 3){
             $error = 'Invalid number of fields in data provided';
            return FALSE;
        } 
        //Check if the passed fields are exactly what is expected
        if (!isset($message['sellerName']) 
                || !isset($message['price'])
                || !isset($message['description'])) {
            $error = 'The passed fields do not correspond to the expected list';
            return FALSE;
        } 
        if(strlen($message['sellerName']) <1 || strlen($message['sellerName']) > 50){
            $error = 'The title is not valid';
            return FALSE;
        }
         if(!is_numeric($message['price']) || $message['price']<0){
           $error = 'The price is not valid';
            return FALSE; 
        }
         if(strlen($message['description']) <5 || strlen($message['description']) > 500){
            $error = 'The description is not valid';
            return FALSE;
        }
       
        return TRUE;
}
$app->get('/messages', function() {
    $messageList = DB::query("SELECT * FROM messages");
    echo json_encode($messageList, JSON_PRETTY_PRINT);
});

$app->delete('/messages/:ID', function($ID) {
    DB::delete('messages', "ID=%d", $ID);
    echo 'true';
});
$app->post('/messages', function() use ($app, $log) {
    $body = $app->request->getBody();
    $record = json_decode($body, TRUE);
    if (!isMessageValid($record, $error)) {
        $log->debug("Failed POST . Invalid data. ".$error);
        $app->response->setStatus(400);
        echo json_encode($error);
        return;
    }
    DB::insert('messages', $record);
    echo DB::insertId();
    // POST / INSERT is special - returns 201
    $app->response->setStatus(201);
});

$app->run();
