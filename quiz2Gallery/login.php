<h3>Login Form</h3>

<?php
require_once 'db.php';

function getForm() {
    $form = <<< ENDTAG
    <form method="POST">
    Email: <input type="email" name="email"><br><br>
    Password: <input type="password" name="password" value=""><br><br>
    <input type ="submit" value="Login" name="submit"> 
</form>  
ENDTAG;
    return $form;
}

if (!isset($_POST['submit'])) {
    //First Show if no data is provided
    echo getForm();
} else {
    //Receiving a submission
    $email = $_POST['email'];
    $password = $_POST['password'];

    $sql = sprintf("Select * from users where email= '%s' and password = '%s'", mysqli_escape_string($conn, $email), mysqli_escape_string($conn, $password));
    $result = mysqli_query($conn, $sql);

    if (!$result) {
        die("Error executing query [$sql] : " . mysqli_error($conn));
    }
    $row = mysqli_fetch_assoc($result);
    if ($row && $row['password'] === $password) {
        
        unset($row['password']);
        $_SESSION['user'] = $row;
        
        echo $row['email'] . " , you are loggedin<br><br>\n";
        echo "<a href=\"index.php\">click to continue</a>";
    } else {
        echo "Login failed<br><br>\n";
        echo getForm();
    }
}

