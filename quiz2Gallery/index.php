<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Gallery</title>
    </head>
    <body>
        
           
            <?php
            require_once 'db.php';
            if (!isset($_SESSION['user'])) {
                echo "You are not logged in.";
                echo '<a href="login.php">Login</a> or <a href="register.php">Register</a>.';

            } else {
                //   echo "Welcome " . $_SESSION['user']['email'] . "!";
                echo 'You may <a href="logout.php">Logout</a> or <a href="addpic.php">Add Picture</a><br>';


            $sql = sprintf("SELECT * FROM pictures WHERE ownerID = '%s'", mysqli_escape_string($conn, $_SESSION['user']['ID']));

            $result = mysqli_query($conn, $sql);
            if (!$result) {
                die("Error executing query [ $sql ] : " . mysqli_error($conn));
            }
            $dataRows = mysqli_fetch_all($result, MYSQLI_ASSOC);
            if(!$dataRows){
               die("<br>No pictures in the gallery yet.");
            }
            echo "<br><table border='1'>";
            echo  "<tr><th>#</th><th>Description</th><th>Picture</th></tr>";
            foreach ($dataRows as $row) {
                $ID = $row['ID'];
                $description = htmlspecialchars($row['description']);
                $picturePath = htmlspecialchars($row['picturePath']);
                
                echo "<tr><td>$ID</td><td>$description</td><td>";
                if (!file_exists($picturePath))
                {
                  echo "The $picturePath path does not exist";
                }else{
                    echo "<a href=\"$picturePath\"><img src=\"$picturePath\" alt=\"Picture\" style=\"width:150px;\"></a></td>";
                }
               echo "</tr>";
            }
            echo "</table>";
            }
            ?>
    </body>
</html>

