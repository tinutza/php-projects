<h3>Add Picture to Gallery</h3>

<?php
require_once 'db.php';
if (!isset($_SESSION['user'])) {
    die("You must log in first if you want to add a picture to the gallery. <a href=\"index.php\">Go to home page</a>");
}

function getForm($desc = '') {
    return <<< ENDTAG
    <form method="POST" enctype="multipart/form-data">
    <input type="file" name="fileToUpload"><br><br>
    Decription: <input type="text" name="description" value="$desc"><br><br>
    <input type ="submit" value="Add Picture" name="submit"> 
</form>  
ENDTAG;
}

if (!isset($_POST['submit'])) {
    echo getForm();
} else {
    //Receiving a submission
    $description = $_POST['description'];
    $target_dir = "uploads/";
    $max_file_size = 5 * 1024 * 1024; // 5000000
//Validate input 
    $errorList = array();

    //Check if there is an image to upload

    if (!$_FILES['fileToUpload']['name']) {
        array_push($errorList, "You must choose an image to upload first");
    } else {
        $fileUpload = $_FILES['fileToUpload'];

        $check = getimagesize($fileUpload['tmp_name']);
        //Check if the file to upload is an image and if it is check if the format is png, gif, bmp, jpg and if it's not  too big
        if (!$check) {
            array_push($errorList, "Error: File upload was not an image file.");
        } elseif (!in_array($check['mime'], array('image/png', 'image/gif', 'image/bmp', 'image/jpeg'))) {
            array_push($errorList, "Error: Only accepting value png,gif,bmp,jpg files.");
        } elseif ($fileUpload['size'] > $max_file_size) {
            array_push($errorList, "Error: File to big, maximuma accepted is $max_file_size bytes");
        }
    }
    //Check if decription is at least 4 characters long
    if (strlen($description) < 4) {
        array_push($errorList, "Description must be at least 4 characters long");
    }
    //Display error messages if invalid data is submitted
    if ($errorList) {
        //submission failed
        echo "<h5>Problems  found in your submission</h5>\n";
        echo "</ul>\n";
        foreach ($errorList as $error) {
            echo "<li>" . htmlspecialchars($error) . "</li>";
        }
        echo "</ul><br><br><br><hr>";
        echo getForm($description);
    } else {
        //submition succesful
        $file_name = preg_replace('/[^A-Za-z0-9\-]/', '_', explode('.', $fileUpload['name'])[0]);
        $file_extension = explode('/', $check['mime'])[1];
        $target_file = $target_dir . date("Ymd-His-") . $file_name . '.' . $file_extension;

        if (move_uploaded_file($fileUpload["tmp_name"], $target_file)) {
            echo "The file " . basename($fileUpload["name"]) . " has been uploaded.";
        } else {
            die("Fatal eror.There was a server-side error on uploading your file.");
        }
        //Insert data into pictures table
        $sql = sprintf("INSERT INTO pictures VALUES (NULL, '%s', '%s', '%s')",
                mysqli_escape_string($conn, $_SESSION['user']['ID']), 
                mysqli_escape_string($conn, $target_file),
                mysqli_escape_string($conn, $description));
        $result = mysqli_query($conn, $sql);
        if (!$result) {
            echo "Error executing query [$sql] : " . mysqli_error($conn);
        } else {
            echo "The Picture was added succesfully<br><br>\n";
            echo "<a href=\"index.php\">Go to home page</a>";
        }
    }
}