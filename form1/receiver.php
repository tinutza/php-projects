<?php

require_once 'db.php';

if (isset($_GET['name']) and (!empty($_GET['name'])) && isset($_GET['age']) and (!empty($_GET['age']))) {
    $name = $_GET['name'];
    $age = $_GET['age'];

    echo "Hello " . $_GET['name'] . "!";
    echo "You are " . $_GET['age'] . " years old";
    
    $sql = "INSERT INTO person VALUES (NULL, '" . mysqli_escape_string($conn, $name). "', ". mysqli_escape_string($conn,$age).")";
    
    $result = mysqli_query($conn, $sql);
    if(!$result){
        echo "Error executing query [$sql] : " .mysqli_error($conn);
    }
} else {
    echo "Provide name and age first";
}