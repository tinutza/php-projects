<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Car Database</title>
    </head>
    <body>
        <a href="caraddedit.php">Add car</a>
        <table border="1">
            <tr>
                <th>#</th>
                <th>Make Model</th>
                <th>Year of Production</th>
                <th>Plates</th>
                <th>Operations</th>
            </tr>
            <?php
        require_once 'db.php';
        $sql = "select * from car";
        $result = mysqli_query($conn, $sql);
if(!$result){
    die("Error executing query [ $sql ] : " .mysqli_error($conn));
}
$dataRows = mysqli_fetch_all($result, MYSQLI_ASSOC);
//print_r($dataRows);
foreach ($dataRows as $row) {
    $ID = $row['ID'];
    $makeModel = htmlspecialchars($row['makeModel']);
    $yop = $row['yop'];
    $plates  =htmlspecialchars($row['plates']);
    echo "<tr><td>$ID</td><td>$makeModel</td><td>$yop</td><td>$plates</td><td><a href=\"cardelete.php?id=$ID\">Delete</a></td></tr>\n";
}
        ?>
        </table>
        
    </body>
</html>
